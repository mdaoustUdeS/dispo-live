from django.shortcuts import render

from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view

@api_view(["GET"])
def item(request):
    items = ["Demo 1", "Demo 2", "Demo 3"]
    return Response(status=status.HTTP_200_OK, data={"data": items})
