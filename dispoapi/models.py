from django.db import models
from django.utils.timezone import now

# Create your models here.


class Dispo(models.Model):
    date = models.DateTimeField(default=now, blank=True)
    busy = models.BooleanField()
    status = models.CharField(max_length=60)

    def __str__(self):
        return self.status
