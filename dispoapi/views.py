from django.shortcuts import render

from rest_framework import viewsets

from .serializers import DispoSerializer
from .models import Dispo
class DispoViewSet(viewsets.ModelViewSet):
    queryset = Dispo.objects.all().order_by('date')
    serializer_class = DispoSerializer
