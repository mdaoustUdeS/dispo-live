from django.apps import AppConfig


class DispoapiConfig(AppConfig):
    name = 'dispoapi'
