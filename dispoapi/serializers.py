from rest_framework import serializers
from .models import Dispo

class DispoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Dispo
        fields = ('date', 'busy', 'status')
